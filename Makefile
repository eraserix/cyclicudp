CC=gcc
CFLAGS=-I. -pthread -Wall
DEPS = util.h
OBJ = cyclicudp.o util.o

all: cyclicudp

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

cyclicudp: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm *.o cyclicudp
