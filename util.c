/* UDP packet latency test software
 * Copyright(c) 2021 Komax AG
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 */

#include "util.h"

#include <net/if.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

static const int64_t nsec_per_sec = 1000000000;
static const int64_t usec_per_sec = 1000000;

int64_t get_nanos() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ts.tv_nsec + ts.tv_sec * nsec_per_sec;
}

void from_nanos(int64_t t, struct timespec *ts) {
    ts->tv_nsec = t % nsec_per_sec;
    t -= ts->tv_nsec;
    ts->tv_sec = t / nsec_per_sec;
}

void sleep_until(int64_t nanos) {
    struct timespec ts;
    from_nanos(nanos, &ts);
    if (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, NULL) < 0) {
        fatal("clock_nanosleep failed");
    }
}

int64_t next_ts(int64_t current) {
    const int64_t interval_ns = 500000;  // 500us
    return current + (interval_ns - current % interval_ns);
}

int64_t calcdiff(struct timespec new, struct timespec old) {
    int64_t diff;
    diff = usec_per_sec * (long long)((int)new.tv_sec - (int)old.tv_sec);
    diff += ((int)new.tv_nsec - (int)old.tv_nsec) / 1000;
    return diff;
}

void print_time() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    printf("%lld.%.9ld\n", (long long)ts.tv_sec, ts.tv_nsec);
}

void fatal(const char msg[]) {
    perror(msg);
    exit(EXIT_FAILURE);
}

int create_udp_socket() {
    int sockfd;
    // Creating socket file descriptor
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        fatal("socket creation failed");
    }
    return sockfd;
}

void bind_socket_to_interface(int sockfd, const char ifname[]) {
    struct ifreq ifr;

    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, ifname, sizeof(ifr.ifr_name));
    if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, &ifr, sizeof(ifr)) <
        0) {
        fatal("bind to interface failed");
    }
}

void open_listen_port(int sockfd, uint16_t port) {
    struct sockaddr_in servaddr;

    memset(&servaddr, 0, sizeof(servaddr));

    servaddr.sin_family = AF_INET;  // IPv4
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(port);

    if (bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) <
        0) {
        fatal("bind failed");
    }
}

struct in_addr get_ip(int sockfd, const char ifname[]) {
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));

    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, ifname, sizeof(ifr.ifr_name));
    if (ioctl(sockfd, SIOCGIFADDR, &ifr) < 0) {
        fatal("get ifaddr failed");
    }

    return ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr;
}
