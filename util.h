/* UDP packet latency test software
 * Copyright(c) 2021 Komax AG
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 */

#ifndef _UTIL_H_
#define _UTIL_H_

#include <netinet/in.h>
#include <stdint.h>
#include <time.h>

int64_t get_nanos();
void from_nanos(int64_t t, struct timespec *ts);
void sleep_until(int64_t nanos);
int64_t next_ts(int64_t current);
int64_t calcdiff(struct timespec new, struct timespec old);

void print_time();
void fatal(const char msg[]);

int create_udp_socket();
void bind_socket_to_interface(int sockfd, const char ifname[]);
void open_listen_port(int sockfd, uint16_t port);
struct in_addr get_ip(int sockfd, const char ifname[]);

#endif  // _UTIL_H_
