# cyclicudp

cyclicudp is a commandline utility that sends a UDP packet from one local
Ethernet port to another and measures the lantency. The cycletime is fixed at
500us. The two ports need to be connected using an ethernet cable.

The loop executes with SCHED_FIFO priority 79 and is mlocked. It performs the
following actions:

1. Get the current monotonic time.
2. Blocking sendto() of the UDP packet with a fixed size.
3. Blocking recv() on the target socket.
4. After reception, get the current monotonic time and update statistics.
5. Sleep until the next interval starts.

## Getting started

Make sure you have build-essential, ethtool and iproute2 installed.

1. Compile the utility by executing `make`.
2. Connect two local Ethernet ports together.
3. Update the script `net-cfg` for your configuration (netdev name,
   configuration, IP, ...)
4. Execute `net-cfg` to setup the ports for realtime UDP traffic.
5. Run `cyclicudp` passing the Ethernet ports to use on the commandline.

By default, the scripts assume the following environment:

- Use the two ports ecat0 and ecat1. Traffic flows from ecat0 to ecat1.
- The ports use the Intel "igb" driver.

## Content

The following executables are part of this repo:

- `net-cfg`: This script sets kernel variables and interface properties to
  enable local transmission of UDP packets and (hopefully) a consistent low
  latency.
- `vnet-cfg`: Configures a veth-pair to help testing the code if lacking two
  connected interfaces.
- `start-trace`: Clears ftrace buffer, enable some events and start tracing.
- `stop-trace`: Stop ftrace.
- `cyclicudp`: main executable that periodically sends a UDP packet. Use --help
  to see commandline options.
- `stress`: run the "stress" utility for cpu, vm, io and hdd with a thread for
  each core as reported by nproc.

## Periodic exection through cron

This installs a cron entry and runs it periodically. The output is logged to the
systemd journal.

1. Checkout the repository at /home/komax/work/cyclicudp
2. `sudo cp /home/komax/work/cyclicudp/cron/cyclicudp /etc/cron.d/`
