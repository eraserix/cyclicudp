/* UDP packet latency test software
 * Copyright(c) 2021 Komax AG
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 */

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <inttypes.h>
#include <limits.h>
#include <pthread.h>
#include <signal.h>
#include <stdarg.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "util.h"

#ifndef ATOMIC_BOOL_LOCK_FREE
#error atomic_bool must be lock free
#endif

struct thread_param {
    int prio;
    struct thread_stat *stats;

    int fromfd;
    int tofd;
    struct sockaddr_in target_host;
};

struct thread_stat {
    int64_t cycles;

    int64_t min;
    int64_t max;
    int64_t act;
    double avg;
};

static const char tracefs_prefix[] = "/sys/kernel/debug/tracing";
static const size_t packetsize = 890;
static const size_t max_path = 256;

static atomic_bool stop;

static uint16_t opt_port = 12345;
static char *opt_txif = "ecat0";
static char *opt_rxif = "ecat1";
static int opt_breaktrace = 0;
static bool opt_tracemark = false;
static int64_t opt_loops = 0;
static bool opt_quiet = false;

static int trace_fd = -1;
static int tracemark_fd = -1;
static int latency_target_fd = -1;

/* Latency trick
 * if the file /dev/cpu_dma_latency exists,
 * open it and write a zero into it. This will tell
 * the power management system not to transition to
 * a high cstate (in fact, the system acts like idle=poll)
 * When the fd to /dev/cpu_dma_latency is closed, the behavior
 * goes back to the system default.
 *
 * Documentation/power/pm_qos_interface.txt
 */
static void set_latency_target(void) {
    const int32_t latency_target_value = 0;
    struct stat s;
    int ret;
    if (stat("/dev/cpu_dma_latency", &s) == 0) {
        latency_target_fd = open("/dev/cpu_dma_latency", O_RDWR);
        if (latency_target_fd == -1) {
            return;
        }
        ret = write(latency_target_fd, &latency_target_value, 4);
        if (ret == 0) {
            printf("# error setting cpu_dma_latency to %d!: %s\n",
                   latency_target_value, strerror(errno));
            close(latency_target_fd);
            return;
        }
        printf("# /dev/cpu_dma_latency set to %dus\n", latency_target_value);
        return;
    }
    return;
}

static void setup_sockets(struct thread_param *p) {
    int fromfd, tofd;

    fromfd = create_udp_socket();
    tofd = create_udp_socket();
    bind_socket_to_interface(fromfd, opt_txif);
    tofd = create_udp_socket();
    bind_socket_to_interface(tofd, opt_rxif);
    open_listen_port(tofd, opt_port);

    struct sockaddr_in servaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(opt_port);
    servaddr.sin_addr = get_ip(tofd, opt_rxif);

    p->fromfd = fromfd;
    p->tofd = tofd;
    p->target_host = servaddr;
}

#define TRACEBUFSIZ 256
static __thread char tracebuf[TRACEBUFSIZ];

static void tracemark(char *fmt, ...) __attribute__((format(printf, 1, 2)));
static void tracemark(char *fmt, ...) {
    va_list ap;
    int len;

    // bail out if we're not tracing
    // or if the kernel doesn't support trace_mark
    if (tracemark_fd < 0) {
        return;
    }

    va_start(ap, fmt);
    len = vsnprintf(tracebuf, TRACEBUFSIZ, fmt, ap);
    va_end(ap);

    // write the tracemark message
    write(tracemark_fd, tracebuf, len);

    // now stop any trace
    write(trace_fd, "0\n", 2);
}

static void set_realtime(int prio) {
    if (mlockall(MCL_CURRENT | MCL_FUTURE) < 0) {
        fatal("mlockall failed");
    }
    char stack[4096];
    memset(stack, 0, sizeof(stack));

    struct sched_param param;
    param.sched_priority = prio;
    if (sched_setscheduler(0, SCHED_FIFO, &param) < 0) {
        fatal("sched_setscheduler failed");
    }
}

static void print_stat(FILE *fp, struct thread_stat *st, bool quiet) {
    if (quiet) {
        return;
    }
    fprintf(fp, "C:%7lu Min:%7ld Act:%8ld Avg:%8ld Max:%8ld\n", st->cycles,
            st->min, st->act, st->cycles ? (int64_t)st->avg / st->cycles : 0,
            st->max);
}

static void *send_receive_thread(void *param) {
    struct thread_param *par = param;
    char buffer[packetsize];
    memset(buffer, 0, packetsize);

    set_realtime(par->prio);

    struct timespec tx, rx;

    while (!atomic_load(&stop)) {
        sleep_until(next_ts(get_nanos()));

        if (clock_gettime(CLOCK_MONOTONIC, &tx) < 0) {
            fatal("clock_gettime failed");
        }
        ssize_t n = sendto(par->fromfd, buffer, sizeof(buffer), 0,
                           (struct sockaddr *)&(par->target_host),
                           sizeof(struct sockaddr_in));
        if (n < 0) {
            fatal("sendto failed");
        } else if (n != packetsize) {
            fatal("sendto did not send enough data");
        }

        n = recv(par->tofd, buffer, sizeof(buffer), 0);
        if (n < 0) {
            fatal("recv failed");
        } else if (n != packetsize) {
            fatal("recv did not get enough data");
        }
        if (clock_gettime(CLOCK_MONOTONIC, &rx) < 0) {
            fatal("clock_gettime failed");
        }
        int64_t act = calcdiff(rx, tx);
        par->stats->act = act;
        if (act < par->stats->min) {
            par->stats->min = act;
        }
        if (act > par->stats->max) {
            par->stats->max = act;
        }
        par->stats->avg += act;
        par->stats->cycles++;

        if (opt_loops && par->stats->cycles == opt_loops) {
            break;
        }
        if (opt_breaktrace && act > opt_breaktrace) {
            tracemark("hit latency threshold (%lu > %d)", act, opt_breaktrace);
            break;
        }
    }

    atomic_store(&stop, true);

    return NULL;
}

static void display_help(bool error) {
    printf("cyclicudp\n");
    printf(
        "Usage:\n"
        "cyclicudp <options>\n\n"
        "--txif=NIC1        net device to send from (default: %s)\n"
        "--rxif=NIC2        net device to receive on (default: %s)\n"
        "--tracemark        write tracemark when -b latency is exceeded\n"
        "--breaktrace=USEC  send break trace command when latency > USEC\n"
        "--port=PORT        bind to UDP port PORT (default: %u)\n"
        "--loops=LOOP       number of loops (default: 0 (endless))\n"
        "--quiet            print a summary only on exit\n"
        "--help             display help message\n",
        opt_txif, opt_rxif, opt_port);

    if (error) {
        exit(EXIT_FAILURE);
    }
    exit(EXIT_SUCCESS);
}

enum option_values {
    OPT_HELP = 1,
    OPT_TXIF,
    OPT_RXIF,
    OPT_TRACEMARK,
    OPT_BREAKTRACE,
    OPT_PORT,
    OPT_LOOPS,
    OPT_QIET,
};

static void process_options(int argc, char *argv[]) {
    static struct option long_options[] = {
        {"help", no_argument, NULL, OPT_HELP},
        {"txif", required_argument, NULL, OPT_TXIF},
        {"rxif", required_argument, NULL, OPT_RXIF},
        {"breaktrace", required_argument, NULL, OPT_BREAKTRACE},
        {"tracemark", no_argument, NULL, OPT_TRACEMARK},
        {"port", required_argument, NULL, OPT_PORT},
        {"loops", required_argument, NULL, OPT_LOOPS},
        {"quiet", no_argument, NULL, OPT_QIET},
        {NULL, 0, NULL, 0}};

    for (;;) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "", long_options, &option_index);
        if (c == -1) {
            break;
        }

        switch (c) {
            case '?':
            case OPT_HELP:
                display_help(0);
                break;
            case OPT_TRACEMARK:
                opt_tracemark = true;
                break;
            case OPT_RXIF:
                opt_txif = optarg;
                break;
            case OPT_TXIF:
                opt_rxif = optarg;
                break;
            case OPT_BREAKTRACE:
                opt_breaktrace = atoi(optarg);
                break;
            case OPT_PORT:
                opt_port = atoi(optarg);
                break;
            case OPT_LOOPS:
                opt_loops = atoi(optarg);
                break;
            case OPT_QIET:
                opt_quiet = true;
                break;
        }
    }

    if (opt_tracemark && !opt_breaktrace) {
        fatal("--tracemark requires --breaktrace");
    }
}

static void set_stop_flag(int sig) { atomic_store(&stop, true); }

int main(int argc, char *argv[]) {
    process_options(argc, argv);

    atomic_init(&stop, false);
    signal(SIGINT, set_stop_flag);

    struct thread_param *p = calloc(1, sizeof(struct thread_param));
    p->stats = calloc(1, sizeof(struct thread_stat));
    p->stats->min = 100000;
    p->stats->max = 0;

    setup_sockets(p);
    p->prio = 79;

    if (opt_tracemark) {
        char path[max_path];
        sprintf(path, "%s/%s", tracefs_prefix, "trace_marker");
        tracemark_fd = open(path, O_WRONLY);
        if (tracemark_fd < 0) {
            fatal("unable to open trace_marker file");
        }
    }

    if (opt_breaktrace) {
        char path[max_path];
        sprintf(path, "%s/%s", tracefs_prefix, "tracing_on");
        trace_fd = open(path, O_WRONLY);
        if (trace_fd < 0) {
            fatal("unable to open tracing_on file");
        }
    }

    set_latency_target();
    pthread_t th;
    if (pthread_create(&th, NULL, send_receive_thread, p) == -1) {
        fatal("pthread_create failed");
    }

    while (!atomic_load(&stop)) {
        print_stat(stdout, p->stats, opt_quiet);
        usleep(10000);
        if (!opt_quiet) {
            printf("\033[1A");
        }
    }
    pthread_join(th, NULL);
    print_stat(stdout, p->stats, false);

    if (latency_target_fd >= 0) {
        close(latency_target_fd);
    }
    close(p->fromfd);
    close(p->tofd);

    free(p->stats);
    free(p);
}
